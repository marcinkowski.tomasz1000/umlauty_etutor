// ==UserScript==
// @name           Skróty do umlautów
// @description	   Dodaje skróty klawiszowe do umlautów na eTutorze
// @author         Tomasz Marcinkowski
// @namespace      https://www.etutor.pl/repetitions
// @version        0.0.5
// @match          *://*www.etutor.pl/repetitions*
// @downloadURL	   https://gitlab.com/marcinkowski.tomasz1000/umlauty_etutor/-/raw/master/umlauty_etutor.user.js
// @updateURL	     https://gitlab.com/marcinkowski.tomasz1000/umlauty_etutor/-/raw/master/umlauty_etutor.user.js
// @homepage       https://gitlab.com/marcinkowski.tomasz1000/umlauty_etutor
// @icon           https://gitlab.com/marcinkowski.tomasz1000/umlauty_etutor/-/raw/master/img/etutor_logo.png
// @grant          none
// ==/UserScript==


window.addEventListener("load", function(){
    checkElement();
});

function checkElement()
{
    var div = document.getElementById("answer");
    if (div === null){
        setTimeout(checkElement, 100);
    }
    else{
        addEvent(div);
    }
}

function addEvent(element){
    element.onkeypress = function(evt)
    {
      if (evt.which)
      {
        var charStr = String.fromCharCode(evt.which);
        var transformedChar = transformTypedChar(charStr);
        if (transformedChar != charStr)
        {
            var start = this.firstElementChild.selectionStart;
            var end = this.firstElementChild.selectionEnd;
            var val = this.firstElementChild.value;
            this.firstElementChild.value = val.slice(0, start) + transformedChar + val.slice(end);

            // Move the caret
            this.firstElementChild.selectionStart = this.firstElementChild.selectionEnd = start + 1;
            return false;
        }
      }
    };
}

function transformTypedChar(charStr)
{
  switch(charStr)
  {
    case "5":
      return "ä";
    case "6":
      return "ö";
    case "7":
      return "ü";
    case "8":
      return "ß";
    case "%":
      return "Ä";
    case "^":
      return "Ö";
    case "&":
      return "Ü";
    case "*":
      return "ß";
    default:
      return charStr;
  }
}
